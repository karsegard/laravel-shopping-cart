<?php

// config for KDA/Laravel\ShoppingCart
return [
    'tables' => [
        'carts' => 'carts',
        'cart_items' => 'cart_items',
    ],

];
