<?php

namespace KDA\Laravel\ShoppingCart\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\ShoppingCart\Models\Cart;

class CartFactory extends Factory
{
    protected $model = Cart::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
