<?php

namespace KDA\Laravel\ShoppingCart\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\ShoppingCart\Models\CartItem;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\KDA\Laravel\ShoppingCart\Models\CartItem>
 */
class CartItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CartItem::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
