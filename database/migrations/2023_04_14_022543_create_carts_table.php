<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\ShoppingCart\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create(ServiceProvider::getTableName('carts'), function (Blueprint $table) {
            //$table->id();
            $table->uuid('id')->primary();
            $table->string('group')->default('');
            $table->string('token')->unique()->nullable();
            $table->nullableNumericMorphs('owner');
            $table->decimal('amount',15,2,true)->default(0);
            $table->decimal('amount_excl_tax',15,2,true)->default(0);
            $table->decimal('tax_amount',15,2,true)->default(0);
            $table->boolean('locked')->default(false);
            $table->timestamps();

        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('carts'));

        Schema::enableForeignKeyConstraints();
    }
};
