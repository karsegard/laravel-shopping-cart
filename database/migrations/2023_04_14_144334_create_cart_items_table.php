<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\ShoppingCart\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ServiceProvider::getTableName('cart_items'), function (Blueprint $table) {
            $table->uuid('id')->primary();
           // $table->unsignedBigInteger('cart_id');
            $table->foreignUuid('cart_id')->references('id')->on(ServiceProvider::getTableName('carts'))->cascadeOnDelete();
            $table->numericMorphs('purchaseable');
            $table->string('label')->nullable();
            $table->integer('qty');
            $table->decimal('unit_price',15,2,true)->nullable();
            $table->decimal('amount',15,2,true)->nullable();
            $table->decimal('tax_amount',8,2,true)->default(0);
            $table->decimal('tax_rate',8,2,true)->default(0);
            $table->boolean('tax_included')->default(true);
            $table->json('meta')->nullable();
          
            $table->timestamps();
        });

        Schema::table(ServiceProvider::getTableName('cart_items'), function (Blueprint $table) {
           
            $table->foreignUuid('parent_id')->nullable()->references('id')->on(ServiceProvider::getTableName('cart_items'))->cascadeOnDelete(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ServiceProvider::getTableName('cart_items'));
    }
};
