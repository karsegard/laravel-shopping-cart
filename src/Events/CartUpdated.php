<?php

namespace KDA\Laravel\ShoppingCart\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use KDA\Laravel\ShoppingCart\Models\Cart;
class CartUpdated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $cart ;
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

}
