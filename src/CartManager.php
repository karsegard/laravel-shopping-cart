<?php

namespace KDA\Laravel\ShoppingCart;

use Closure;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Traits\Conditionable;
use Illuminate\Support\Traits\Tappable;
use KDA\Laravel\ShoppingCart\Concerns\Authenticated;
use KDA\Laravel\ShoppingCart\Concerns\CanCreate;
use KDA\Laravel\ShoppingCart\Concerns\EvaluatesClosure;
use KDA\Laravel\ShoppingCart\Concerns\HasGroup;
use KDA\Laravel\ShoppingCart\Concerns\HasRequest;
use KDA\Laravel\ShoppingCart\Concerns\HasVisibility;
use KDA\Laravel\ShoppingCart\Concerns\MinimalOrder;
use KDA\Laravel\ShoppingCart\Models\Cart;

//use Illuminate\Support\Facades\Blade;
class CartManager
{

    use EvaluatesClosure;
    use Tappable;
    use Conditionable;
    use Authenticated;
    use HasRequest;
    use HasVisibility;
    use HasGroup;
    use CanCreate;
    use MinimalOrder;
    
    protected Cart|null|Closure $cart = null;
  

    public function __construct()
    {
        $this->setUp(); 
    }

    public function setUp(){
        $this->authenticated($this->getDefaultAuthenticatedClosure());
    }
  
    public function addItem($item, $count,$parent_id=null): static
    {
        $cart = $this->getCart();
        $attr = [];
        if(!blank($parent_id)){
            $attr['parent_id']=$parent_id;
        }
        $cart->addItem($item, $count,$attr);
        return $this;
    }

    public function getCart(): ?Cart
    {
        $cart = $this->evaluate($this->cart,$this->getEvaluationParameters(['cart']));

        if (!$cart) {
            //load cart from cookie first;
            $this->tap(fn ($manager) => $manager->load());
            $cart = $this->cart;
        }

        if (!$cart && $this->shouldCreate()) {
            $this->cart($this->createCart());
            $cart = $this->evaluate($this->cart);
        }

        return $cart;
    }

    public function cart(Closure |Cart| null $cart): static
    {
        $this->cart = $cart;
        return $this;
    }


    public function createCart(): Cart
    {
        if ($this->isAuthenticated()) {
            return $this->createAuthenticatedCart();
        } else {
            return $this->createAnonymousCart();
        }
    }

    public function createAnonymousCart(): Cart
    {
        $cart =  Cart::createAnonymous($this->getGroup());
        Cookie::queue(Cookie::make($this->getCookieName(), $cart->token, $this->getCookieExpiration()));
        return $cart;
    }

    public function removeCookie(): static
    {
        Cookie::queue(Cookie::forget($this->getCookieName()));
        //Cookie::queue(Cookie::make($this->getCookieName(), $cart->token, $this->getCookieExpiration()));
        return $this;
    }

    public function getCookieName()
    {
        return 'cart_' . $this->getGroup();
    }

    public function getCookieExpiration()
    {
        return 120;
    }


    public function load(): static
    {
        $anonymous = $this->getAnonymousCart();
        $authenticated = null;
        $cart = null;
        if ($this->isAuthenticated()) {
            $authenticated = $this->getAuthenticatedCart();
        }
        if ($this->isAuthenticated()) {
            if ($anonymous && $authenticated) {
                $cart =  $this->merge($anonymous, $authenticated);
            } else if ($anonymous) {
                $anonymous->owner()->associate($this->getUser())->save();
                $this->removeCookie();
                $cart = $anonymous;
            } else if ($authenticated) {
                $cart = $authenticated;
            }
        }else {
            $cart = $anonymous;
        }
        $this->cart($cart);
        return $this;
    }

    public function merge($anonymous, $authenticated)
    {
        foreach($anonymous->items as $item){
            $authenticated->addItem($item->purchaseable,$item->qty);
        }
        $anonymous->delete();
        $this->removeCookie();
        return $authenticated;
    }

    public function getAnonymousCart()
    {
        $token = $this->getRequest()?->cookie($this->getCookieName());

        $cart = null;

        if (!blank($token)) {
            $cart = Cart::unlocked()->forGroup($this->getGroup())->where('token', $token)->first();
        }

        return $cart;
    }

    


}
