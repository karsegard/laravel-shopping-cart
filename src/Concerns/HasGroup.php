<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait HasGroup
{
    protected string | Closure $group = '';
    public function getGroup()
    {
        return $this->evaluate($this->group, $this->getEvaluationParameters(['group']));
    }

    public function group(Closure | string | null $group = ''): static
    {
        if (blank($group)) {
            $group = '';
        }
        $this->cart = null;
        $this->group = $group;
        return $this;
    }
}
