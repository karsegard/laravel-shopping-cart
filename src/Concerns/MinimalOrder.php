<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait MinimalOrder
{
    protected Closure| float | null $minimal_order = null;
  
    public function minimalOrder(Closure| float | null $minimal_order):static
    {
        $this->minimal_order= $minimal_order;
        return $this;
    }

    public function getMinimalOrder():?float
    {
        return $this->evaluate($this->minimal_order,$this->getEvaluationParameters());
    }

    public function hasMinimalOrder():bool
    {

        $minimal_order = $this->getMinimalOrder();
        return $minimal_order !=null && $minimal_order > 0;
    }
}
