<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Auth\Authenticatable;
use KDA\Laravel\ShoppingCart\Models\Cart;

trait Authenticated
{
    protected null|Closure $authenticated = null;
  
    
    public function createAuthenticatedCart()
    {
        return Cart::createOwned($this->getUser(), $this->getGroup());
    }

    public function isAuthenticated()
    {
        return !blank($this->getUser());
    }

    public function getUser(): ?Authenticatable
    {
        return $this->evaluate($this->authenticated, $this->getEvaluationParameters());
    }

    public function authenticated(Authenticatable | Closure $authenticated): static
    {
        $this->authenticated = $authenticated;
        return $this;
    }

    public function getDefaultAuthenticatedClosure():Closure
    {
        return function(){
            return auth()->user();
        };
    }

    public function getAuthenticatedCart()
    {
        return Cart::unlocked()->forGroup($this->getGroup())->forOwner($this->getUser())->first();
    }
}
