<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait HasRequest
{
    protected ?Request $request = null;
    public function getRequest(): ?Request
    {
        return $this->evaluate($this->request, $this->getEvaluationParameters(['request']));
    }

    public function request(?Request $request = null): static
    {
        $this->request = $request;
        return $this;
    }
}
