<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait HasVisibility
{
    protected Closure| bool $visible = true;
  
    public function visible(Closure | bool $visible):static
    {
        $this->visible= $visible;
        return $this;
    }

    public function isVisible():bool
    {
        return $this->evaluate($this->visible);
    }
}
