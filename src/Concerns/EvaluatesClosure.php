<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
trait EvaluatesClosure
{
    public function evaluate($value, array | Collection $parameters = [])
    {
        if($parameters instanceof Collection){
            $parameters = $parameters->all();
        }
        if ($value instanceof Closure) {
            return app()->call(
                $value,
                $parameters
            );
        }

        return $value;
    }

    public function getEvaluationParameters(array $exceptKeys=[]):Collection
    {
        $args =  collect([
           
        ])->when(!in_array('manager',$exceptKeys),function($collection){
            return $collection->put('manager',$this);
        })->when(!in_array('group',$exceptKeys),function($collection){
            return $collection->put('group',$this->getGroup());
        });
        return $args;
    }
    
}
