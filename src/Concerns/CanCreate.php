<?php

namespace KDA\Laravel\ShoppingCart\Concerns;


use Closure;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait CanCreate
{
    protected bool $should_create = false;
    

    public function create(bool $create = true): static
    {
        $this->should_create = $create;
        return $this;
    }

    public function shouldCreate(): bool
    {
        return $this->should_create;
    }

}
