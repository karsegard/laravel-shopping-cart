<?php

namespace KDA\Laravel\ShoppingCart;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\ShoppingCart\CartManager as Library;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\ShoppingCart\Facades\CartManager as Facade;
use KDA\Laravel\ShoppingCart\Models\Cart;
use KDA\Laravel\ShoppingCart\Models\CartItem;
use KDA\Laravel\ShoppingCart\Observers\CartItemObserver;
use KDA\Laravel\ShoppingCart\Observers\CartObserver;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasConfigurableTableNames;
use KDA\Laravel\Traits\HasDumps;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasMigration;
use KDA\Laravel\Traits\HasObservers;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasLoadableMigration;
    use HasMigration;
    use HasDumps;
    use HasConfigurableTableNames;
    use HasObservers;
    protected $packageName = 'shopping-cart';
    protected $observers = [
        [CartItem::class=>CartItemObserver::class],
        [Cart::class=>CartObserver::class]
    ];
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    // trait \KDA\Laravel\Traits\HasConfig;
    //    registers config file as
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';

    protected $configs = [
        'kda/shopping-cart.php' => 'kda.shopping-cart',
    ];

    public function getDumps(): array
    {
        return config(static::$tables_config_key);
    }

    protected static $tables_config_key = 'kda.shopping-cart.tables';

    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
