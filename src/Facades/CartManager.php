<?php

namespace KDA\Laravel\ShoppingCart\Facades;

use Illuminate\Support\Facades\Facade;

class CartManager extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
