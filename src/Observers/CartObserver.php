<?php

namespace KDA\Laravel\ShoppingCart\Observers;

use KDA\Laravel\ShoppingCart\Events\CartUpdated;
use KDA\Laravel\ShoppingCart\Models\Cart;

class CartObserver
{

    public function created(Cart $item)
    {
        event( new CartUpdated($item));
    }

    public function updated(Cart $item)
    {
        event( new CartUpdated($item));
    }
    
}
