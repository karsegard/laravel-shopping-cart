<?php

namespace KDA\Laravel\ShoppingCart\Observers;

use KDA\Laravel\ShoppingCart\Events\CartItemDeleted;
use KDA\Laravel\ShoppingCart\Events\CartItemUpdated;
use KDA\Laravel\ShoppingCart\Models\CartItem;

class CartItemObserver
{
    public function created(CartItem $item)
    {
       /* $item->updateTotal();
        $item->updateTaxRate();
        $item->parent?->updateTotal();
        $item->parent?->updateTaxRate();
        $item->cart->updateTotal();
        $item->cart->updateTaxTotal();*/
        CartItem::withoutEvents(function() use ($item){
            $item->updateTotal()->updateTaxRate()->save();
            $item->parent?->updateTotal()->updateTaxRate()->save();
        });
        $item->cart->updateTotal()->updateTaxTotal()->save();
        event( new CartItemUpdated($item));
    }

    public function updated(CartItem $item)
    {
        /*$item->updateTotal();
        $item->updateTaxRate();
        $item->parent?->updateTotal();
        $item->parent?->updateTaxRate();
        $item->cart->updateTotal();
        $item->cart->updateTaxTotal();*/
        CartItem::withoutEvents(function() use ($item){
            $item->updateTotal()->updateTaxRate()->save();
            $item->parent?->updateTotal()->updateTaxRate()->save();
        });
        $item->cart->updateTotal()->updateTaxTotal()->save();
        event( new CartItemUpdated($item));
    }

    public function deleted(CartItem $item){
        /*$item->parent?->updateTotal();
        $item->parent?->updateTaxRate();
        $item->cart->updateTotal();
        $item->cart->updateTaxTotal();*/
        CartItem::withoutEvents(function() use ($item){
            $item->parent?->updateTotal()->updateTaxRate()->save();
        });
        $item->cart->updateTotal()->updateTaxTotal()->save();
        event( new CartItemDeleted($item));
    }
}
