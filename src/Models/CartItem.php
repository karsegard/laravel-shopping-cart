<?php

namespace KDA\Laravel\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ShoppingCart\Database\Factories\CartItemFactory;
use KDA\Laravel\ShoppingCart\ServiceProvider;

class CartItem extends Model
{
    use HasFactory;
    use HasUuids;

    protected $fillable = [
        'qty',
        'cart_id',
        'purchaseable_type',
        'purchaseable_id',
        'unit_price',
        'tax_included',
        'tax_rate',
        'parent_id',
        'meta'
    ];

    protected $casts = [
        'amount' => 'decimal:2',
        'unit_price' => 'decimal:2',
        'meta' => 'json'
    ];


    public function getTable()
    {
        return ServiceProvider::getTableName('cart_items');
    }


    protected static function newFactory()
    {
        return CartItemFactory::new();
    }

    /**
     * RELATIONSHIPS
     */

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
    public function purchaseable()
    {
        return $this->morphTo();
    }

    public function children()
    {
        return $this->hasMany(CartItem::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(CartItem::class);
    }

    /**attributes */
    protected function qty(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value < 0 ? 0 : $value,
            set: fn ($value) => $value < 0 ? 0 : $value
        );
    }

    public function getRealQtyAttribute(): int
    {
        if ($this->parent) {
            return $this->parent->qty * $this->qty;
        }
        return $this->qty;
    }
    /**
     * scopes
     */

    public function scopeForCart($q, $cart)
    {
        if ($cart instanceof Cart) {
            $cart = $cart->getKey();
        }
        return $q->where('cart_id', $cart);
    }
    public function scopeForPurchaseable($q, $purchaseable)
    {

        return $q->where('purchaseable_id', $purchaseable->getKey())->where('purchaseable_type', get_class($purchaseable));
    }

    public function scopeNotEmpty($q)
    {
        return $q->where('qty', '>', 0);
    }
    public function scopeRoot($q)
    {
        return $q->whereNull('parent_id');
    }

    public function scopeForPurchaseableTypes($q, $itemClasses)
    {
        return $q->where(function ($q) use ($itemClasses) {
            foreach ($itemClasses as $item) {
                $q->orWhere('purchaseable_type', $item);
            }
        });
    }
    /**
     * methods
     */

    public function updateTotal(): static
    {
        //self::withoutEvents(function () {
        $this->amount = $this->qty * $this->unit_price;
        $children_amount = $this->children->reduce(function ($carry, $item) {
            $carry += $this->qty * $item->amount;
            return $carry;
        }, 0);
        $this->amount += $children_amount;
        //    $this->save();
        //});
        return $this;
    }

    public function updateTaxRate(): static
    {
        //self::withoutEvents(function () {
        if ($this->tax_included) {
            //      $this->tax_amount = $this->amount - ($this->unit_price / (1 + ($this->tax_rate / 100))) * $this->qty;
            $this->tax_amount = ($this->unit_price - ($this->unit_price / (1 + ($this->tax_rate / 100)))) * $this->qty;
        } else {
            //       $this->tax_amount = ($this->unit_price * $this->tax_rate / 100) * $this->qty;
            $this->tax_amount = ($this->unit_price * $this->tax_rate / 100) * $this->qty;
        }
        //    $this->save();
        //});
        return $this;
    }

    /**static method  */

    public static function retrievePurchaseableAttributes($purchaseable): array
    {
        $attrs = [];

        $price = $purchaseable->getPurchaseablePrice();
        $taxed = $purchaseable->isPurchaseableTaxIncluded() ?? false;
        $tax = $purchaseable->getPurchaseableTax() ?? 0.0;
        $attrs = [
            'unit_price' => $price,
            'tax_included' => $taxed,
            'tax_rate' => $tax,
        ];
        return $attrs;
    }

    /**attributes */

    public function amount(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->tax_included ? $value : $value + $this->tax_amount,
            set: function ($value, $attributes) {
                $attributes['amount'] = $value;
                return $attributes;
            }
        );
    }

    public function amount_without_tax(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->tax_included ? $value - $this->tax_amount : $value,

        );
    }

    public function getLabelAttribute()
    {
        return $this->purchaseable->getPurchaseableName();
    }
}
