<?php

namespace KDA\Laravel\ShoppingCart\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ShoppingCart\Facades\CartManager;
use KDA\Laravel\ShoppingCart\Database\Factories\CartFactory;
use KDA\Laravel\ShoppingCart\ServiceProvider;

class Cart extends Model
{
    use HasFactory;
    use HasUuids;

    protected $fillable = [
        'id',
        'token',
        'group',
        'owner_id',
        'owner_type',
        'locked',
        'amount',
        'amount_excl_tax',
        'tax_amount',
    ];

    protected $appends = [];

    protected $casts = [
        'locked' => 'boolean',
        'meta' => 'json',
        'amount' => 'decimal:2',
        'amount_excl_tax' => 'decimal:2',
        'tax_amount' => 'decimal:2',
    ];

    public function getTable()
    {
        return ServiceProvider::getTableName('carts');
    }


    protected static function newFactory()
    {
        return CartFactory::new();
    }

    /**
     * Attributes
     */

    public function productsCount(): Attribute
    {
        $this->load('items');
        return Attribute::make(
            get: fn ($value) => $this->items->reduce(function ($carry, $item) {
                $carry += $item->qty;
                return $carry;
            }, 0)
        );
    }
    /**
     * RELATIONSHIPS
     */

    public function owner()
    {
        return $this->morphTo();
    }

    public function items()
    {
        return $this->hasMany(CartItem::class)->orderBy('id');
    }

    public function cartItems()
    {
        return $this->items()->notEmpty()->root();
    }

    /**
     * scopes
     */

    public function scopeUnlocked($q)
    {
        return $q->where('locked', false);
    }

    public function scopeLocked($q)
    {
        return $q->where('locked', true);
    }

    public function scopeForOwner($q, $owner)
    {
        return $q->where('owner_id', $owner->getKey())->where('owner_type', get_class($owner));
    }

    public function scopeForGroup($q, $group = '')
    {
        if (blank($group)) {
            $group = '';
        }
        return $q->where('group', $group);
    }
    /**
     * METHODS
     */
    public function lock(): static
    {
        $this->locked =  true;
        return $this;
    }

    public function updateTotal(): static
    {

        //self::withoutEvents(function () {
        $this->load('items');
        $amount = $this->items->reduce(function ($carry, $item) {
            if (blank($item->parent_id)) {
                $carry += $item->amount;
            }
            return $carry;
        }, 0);
        $this->amount = $amount;
        // $this->save();
        //});
        return $this;
    }

    public function getDiscountableTotal()
    {
        return  $this->items->reduce(function ($carry, $item) {
            if (blank($item->parent_id) && $item->purchaseable->isDiscountable()) {
                $carry += $item->amount;
            }
            return $carry;
        }, 0);
    }

    public function hasUndiscountableProduct()
    {
        return $this->items->filter(fn ($item) => !$item->purchaseable->isDiscountable())->count() > 0;
    }


    public function updateTaxTotal(): static
    {

        //self::withoutEvents(function () {
        $this->load('items');
        $tax_amount = $this->items->reduce(function ($carry, $item) {
            $carry += $item->tax_amount;
            return $carry;
        }, 0);
        $this->tax_amount = $tax_amount;
        $this->amount_excl_tax = $this->amount - $this->tax_amount;
        //  $this->save();
        //});
        return $this;
    }

    public function hasPurchaseable($purchaseable)
    {
        return $this->items->where('purchaseable_type', get_class($purchaseable))->where('purchaseable_id', $purchaseable->getKey())->count() > 0;
    }

    public function getItemFromPurchaseable($purchaseable)
    {
        return $this->items->where('purchaseable_type', get_class($purchaseable))->where('purchaseable_id', $purchaseable->getKey())->first();
    }

    public function addItem($purchaseable, $qty = 1, $attr = [])
    {
        if ($this->locked) {
            throw new \Exception('Cart is locked');
        }
        if (($purchaseable instanceof CartItem)) {
            $item = $purchaseable;
            $purchaseable = $item->purchaseable;
        } else {
            $item = CartItem::forCart($this)->forPurchaseable($purchaseable)->first();
        }


        if (!$item) {
            $attr = array_merge(
                CartItem::retrievePurchaseableAttributes($purchaseable),
                $attr,
            );
            $item = new CartItem($attr);
            $item->qty = $qty;
            $item->cart()->associate($this);
            $item->purchaseable()->associate($purchaseable);
        } else {
            $item->qty += $qty;
        }

        $item->save();
    }

    public function removeItem($item, $qty = 1)
    {
        if ($this->locked) {
            throw new \Exception('Cart is locked');
        }
        if (!($item instanceof CartItem)) {
            $item = CartItem::forCart($this)->forPurchaseable($item)->first();
        }
        if ($item) {

            $item->qty -= $qty;
            if ($item->qty == 0) {
                $item->delete();
            } else {
                $item->save();
            }
        }
    }

    public function updateItem($item, $qty, $attr = [])
    {
        if ($this->locked) {
            throw new \Exception('Cart is locked');
        }
        if (!($item instanceof CartItem)) {
            $item = CartItem::forCart($this)->forPurchaseable($item)->first();
        }
        if ($item) {
            $item->fill($attr);
            $item->qty = $qty;
            if ($item->qty == 0) {
                $item->delete();
            } else {
                $item->save();
            }
        }
    }

    /**
     * STATIC METHODS
     */
    public static function createAnonymous($group = '')
    {
        /* $c = (new static);
        $c->token = (string) \Str::uuid();
        $c->group = $group;*/
        $c = static::create([
            'group' => $group,
            'token' => (string) \Str::uuid()
        ]);
        $c->save();

        return $c;
    }

    public static function createOwned($user, $group = '')
    {
        $c = (new static);
        $c->owner()->associate($user);
        $c->group = $group;
        $c->save();
        return $c;
    }

    public function getItemOfTypes($classes)
    {
        return CartItem::forCart($this)->forPurchaseableTypes($classes)->get();
    }

    public function hasMinimalOrder()
    {
        return CartManager::create(false)->cart($this)->hasMinimalOrder();
    }

    public function getMinimalOrder()
    {
        return number_format(CartManager::create(false)->cart($this)->getMinimalOrder(), 2);
    }

    public function canOrder()
    {
        return $this->products_count > 0 && (!$this->hasMinimalOrder() ||  $this->getMinimalOrder() <= $this->amount);
    }

    public function clone(?array $item_types = null): Cart
    {
        $newCart = $this->replicate();
        $newCart->token=null;
        $newCart->save();
        $itemsToReplicate = blank($item_types) ? $this->items : $this->getItemOfTypes($item_types);

        $itemsToReplicate = $itemsToReplicate->sortBy('parent_id');

        $keymap = [];
        foreach ($itemsToReplicate as $key => $item) {
            $replica = $item->replicate()->fill([
                'cart_id' => $newCart->getKey()
            ]);
            $new_parent_id = null;
            if (!blank($item->parent_id)) {
                dump($item->parent_id);
                $new_parent_id = $keymap[$item->parent_id];
            }
            $replica->parent_id = $new_parent_id;
            $replica->save();
            $keymap[$item->getKey()] = $replica->getKey();
        }
        $newCart->updateTotal()->updateTaxTotal()->save();
        return $newCart;
    }
}
