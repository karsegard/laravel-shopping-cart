<?php
namespace KDA\Laravel\ShoppingCart\Models\Contracts;


interface Purchaseable{

    public function getPurchaseablePrice():float;
    public function isPurchaseableTaxIncluded():bool;
    public function getPurchaseableTax():float;
    public function getPurchaseableName():string;
    public function isDiscountable(): bool;
    
}
