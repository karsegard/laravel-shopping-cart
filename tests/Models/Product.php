<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ShoppingCart\Models\Contracts\Purchaseable;
use KDA\Tests\Database\Factories\ProductFactory;

class Product extends Model implements Purchaseable
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'tax_included',
        'tax_rate'
    ];

    protected $casts = [
        'tax_included' => 'boolean'
    ];

    protected static function newFactory()
    {
        return ProductFactory::new();
    }

    public function getPurchaseablePrice(): float
    {
        return $this->price;
    }
    public function isPurchaseableTaxIncluded(): bool
    {
        return $this->tax_included;
    }
    public function getPurchaseableTax(): float
    {
        return $this->tax_rate;
    }
    public function getPurchaseableName(): string
    {
        return $this->name;
    }

    public function isDiscountable(): bool
    {
        return true;
    }
}
