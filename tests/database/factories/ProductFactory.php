<?php

namespace KDA\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\Product;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'price' => $this->faker->randomFloat(2,1,299),
            'tax_included'=>$this->faker->boolean(),
            'tax_rate'=>$this->faker->randomFloat(2,0,45)
        ];
    }
}
