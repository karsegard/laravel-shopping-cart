<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\ShoppingCart\Events\CartUpdated;
use KDA\Laravel\ShoppingCart\Models\Cart;
use Illuminate\Support\Facades\Event;
use KDA\Laravel\ShoppingCart\Events\CartItemUpdated;
use KDA\Laravel\ShoppingCart\Facades\CartManager;
use KDA\Laravel\ShoppingCart\Models\CartItem;
use KDA\Tests\Models\Product;
use KDA\Tests\TestCase;

class EventsTest extends TestCase
{
  use RefreshDatabase;

  /** @test */
  public function model_created()
  {
    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Cart::setEventDispatcher($initialEvent);
    CartItem::setEventDispatcher($initialEvent);

    
    $p = Product::factory()->create(['name' => 'Apples']);
    CartManager::create(true)->addItem($p, 1)->getCart();


    Event::assertDispatched(CartItemUpdated::class, function (CartItemUpdated $event) {
      
      return true;
    });

    Event::assertDispatched(CartUpdated::class, function (CartUpdated $event) {
      
      return true;
    });
  }
}
