<?php

namespace KDA\Tests\Behat\Context\Concerns;

use Behat\Gherkin\Node\PyStringNode;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ShoppingCart\Facades\CartManager;
use KDA\Laravel\ShoppingCart\Models\Cart as ModelsCart;
use KDA\Tests\Models\Product;

/**
 * Defines application features from the specific context.
 */
trait Cart
{
    protected ?Model $product = null;
    protected ?Model $option = null;
    protected ?ModelsCart $cart = null;
    /**
     * @Given A product named :name
     */
    public function aProductNamed($name)
    {
        $this->product = Product::factory()->create(['name' => $name]);
        $this->assertNotNull($this->product);
    }

    /**
     * @When I add :count product to the cart
     */
    public function iAddProductToTheCart($count)
    {
        $this->cart = CartManager::create(true)->addItem($this->product, $count)->getCart();
        $this->assertNotNull($this->cart);
    }

    /**
     * @Then I have :count product in the cart
     */
    public function iHaveProductInTheCart($count)
    {
        $_count = $this->cart->productsCount;
        $this->assertEquals($count, $_count);
    }
    /**
     * @Then I have :count items in the cart
     */
    public function iHaveItemsInTheCart($count)
    {
        $_count = $this->cart->items->count();
        $this->assertEquals($count, $_count);
    }

    /**
     * @Given a product with the following factory attributes
     */
    public function aProductWithTheFollowingFactoryAttributes(PyStringNode $string)
    {
        $this->product = Product::factory()->create(json_decode($string->getRaw(), true));
    }
    /**
     * @Then The price of the cart is :total
     */
    public function thePriceOfTheCartIs($total)
    {
        $this->assertEquals($total, $this->cart->amount);
    }

    /**
     * @Then The tax amount is :total
     */
    public function theTaxAmountIs($total)
    {
        $this->assertEquals($total, $this->cart->tax_amount);
    }

    /**
     * @Given an option with the following factory attributes
     */
    public function anOptionWithTheFollowingFactoryAttributes(PyStringNode $string)
    {
        $this->option = Product::factory()->create(json_decode($string->getRaw(), true));
        $this->assertNotNull($this->option);
    }

    /**
     * @Given I add :count option to the cart
     */
    public function iAddOptionToTheCart($count)
    {
        $cart = $this->cart;
        $parent = $cart->items->where('purchaseable_type', Product::class)->where('purchaseable_id', $this->product->getKey())->first();
        $this->cart = CartManager::create(false)->addItem($this->option, $count, $parent->getKey())->getCart();

        $this->assertNotNull($this->cart);
    }
}
