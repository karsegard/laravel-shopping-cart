Feature: Children
    
  
    Scenario: A product with an option, tax included
    Given a product with the following factory attributes 
    """
    {
        "name":"Product A",
        "price":"12.50",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "1" product to the cart
    Given an option with the following factory attributes 
    """
    {
        "name":"Option A",
        "price":"10.00",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "1" option to the cart

    Then I have "2" product in the cart
    And I have "2" items in the cart
    And The price of the cart is "22.50"
    And The tax amount is "1.60"


    Scenario: A product with two option, tax included
    Given a product with the following factory attributes 
    """
    {
        "name":"Product A",
        "price":"12.50",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "1" product to the cart
    Given an option with the following factory attributes 
    """
    {
        "name":"Option A",
        "price":"10.00",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "2" option to the cart

    Then I have "3" product in the cart
    And I have "2" items in the cart
    And The price of the cart is "32.50"
    And The tax amount is "2.32"
