Feature: Cart
    
    Scenario: Adding a product to the cart
    Given A product named "Product A" 
    When I add "1" product to the cart
    Then I have "1" product in the cart

    Scenario: Adding several products to the cart
    Given A product named "Product A" 
    And I add "1" product to the cart
    Given A product named "Product B" 
    And I add "3" product to the cart
    Then I have "4" product in the cart

    Scenario: Adding same product twice just update quantity
    Given A product named "Product A" 
    And I add "1" product to the cart
    And I add "1" product to the cart
   
    Then I have "2" product in the cart
    And I have "1" items in the cart


    Scenario: Product with tax
    Given a product with the following factory attributes 
    """
    {
        "name":"Product A",
        "price":"12.50",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "1" product to the cart
    Then I have "1" product in the cart
    And I have "1" items in the cart
    And The price of the cart is "12.50"
    And The tax amount is "0.89"

    Scenario: Products with taxes
    Given a product with the following factory attributes 
    """
    {
        "name":"Product A",
        "price":"12.50",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "2" product to the cart
    Then I have "2" product in the cart
    And I have "1" items in the cart
    And The price of the cart is "25.00"
    And The tax amount is "1.79"

    Scenario: Product with tax
    Given a product with the following factory attributes 
    """
    {
        "name":"Product A",
        "price":"12.50",
        "tax_included":false,
        "tax_rate":"7.7"
    }
    """
    And I add "1" product to the cart
    Then I have "1" product in the cart
    And I have "1" items in the cart
    And The price of the cart is "13.46"
    And The tax amount is "0.96"


    Scenario: Mixed Prducts with and without tax
    Given a product with the following factory attributes 
    """
    {
        "name":"Product A",
        "price":"12.50",
        "tax_included":false,
        "tax_rate":"7.7"
    }
    """
    And I add "1" product to the cart
    Given a product with the following factory attributes 
    """
    {
        "name":"Product B",
        "price":"10.00",
        "tax_included":true,
        "tax_rate":"7.7"
    }
    """
    And I add "1" product to the cart

    Then I have "2" product in the cart
    And I have "2" items in the cart
    And The price of the cart is "23.46"
    And The tax amount is "1.67"
