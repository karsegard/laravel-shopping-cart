# kda/laravel-shopping-cart

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-shopping-cart.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-shopping-cart)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-shopping-cart.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-shopping-cart)

## Installation

You can install the package via composer:

```bash
composer require kda/laravel-shopping-cart
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\ShoppingCart\ServiceProvider" --tag="migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="\KDA\Laravel\ShoppingCart\ServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
];
```

## Usage

### Preparing your Models

```php
<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ShoppingCart\Models\Contracts\Purchaseable;

class Product extends Model implements Purchaseable
{
    // ...

    public function getPurchaseablePrice(): float
    {
        return $this->price;
    }
    public function isPurchaseableTaxIncluded(): bool
    {
        return $this->tax_included;
    }
    public function getPurchaseableTax(): float
    {
        return $this->tax_rate;
    }
    public function getPurchaseableName(): string
    {
        return $this->name;
    }

    public function isDiscountable(): bool
    {
        return true;
    }
}


```

### Retrieving the cart

```php

//retrieve the cart (if existing)
CartManager::create(false)->group('specific')->getCart();

//retrieve the cart (created if not existing)
CartManager::create(true)->group('specific')->getCart();

```

### Adding Products

```php

//adding a product

$item = //retrieve your item with purchaseable contract;
CartManager::group('specific')->addItem($item,2);

//add purchaseable as subitem
$option = //retrieve your item with purchaseable contract;

CartManager::group('specific')->addItem($option,1,$item->getKey())
```

Note that the children record amount and quantities will always be for 1 parent record.
Parent total amount is updated with the correct children amount and quantities

ATM the cart cannot have several parent purchaseable with different children, but it's planned.

## About Taxes

Tax is mandatory but you can set it to 0;

Taxes amount are automatically calculated when adding product depending of your `Purchaseable` methods.

The amount stored in the `cart_items` table is never modified from the original value.
The `amount` attribute of the model will return the price with tax if the tax is not included.
The `amount_without_tax` will return the price with tax removed if tax is included.

This avoids calculation errors while preserving the original data. While simplifying frontend logic when displaying.

## Anonymous vs Authenticated

When creating a cart while not authenticated, the lib will return a cookie. So you should do it in a controller or returning a response after doing it. It works fine with livewire.

By default the lib is using auth()->user() to retrieve the autenticated user but you can modify this behavior by adding something like this in the `boot` method of a provider

```php
use KDA\Laravel\ShoppingCart\Facades\CartManager;

CartManager::authenticated(function($manager){
    return auth('myguard')->user();
});
```

When the customer authenticates and have a cart cookie, the anonymous cart will be merged with the user's current cart (if any)

## Testing

```bash
composer test

vendor/bin/behat
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](SECURITY.md) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](CONTRIBUTORS.md)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
Please see [License File](LICENSE.KDA.md) for more information.
